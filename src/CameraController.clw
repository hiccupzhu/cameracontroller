; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCameraControllerDlg
LastTemplate=CEdit
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "CameraController.h"

ClassCount=12
Class1=CCameraControllerApp
Class2=CCameraControllerDlg
Class3=CAboutDlg

ResourceCount=4
Resource1=IDD_CameraController_DIALOG
Resource2=IDR_MAINFRAME
Class4=CExLabel
Class5=CExButton
Class6=CBaseButton
Class7=CImgButton
Class8=CDownUpButton
Class9=CDlgCover
Resource3=IDD_DLG_COVER
Class10=CDlgSet
Class11=CLinkLabel
Class12=CExEdit
Resource4=IDD_DLG_SET

[CLS:CCameraControllerApp]
Type=0
HeaderFile=CameraController.h
ImplementationFile=CameraController.cpp
Filter=N

[CLS:CCameraControllerDlg]
Type=0
HeaderFile=CameraControllerDlg.h
ImplementationFile=CameraControllerDlg.cpp
Filter=W
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CCameraControllerDlg

[CLS:CAboutDlg]
Type=0
HeaderFile=CameraControllerDlg.h
ImplementationFile=CameraControllerDlg.cpp
Filter=D

[DLG:IDD_CameraController_DIALOG]
Type=1
Class=CCameraControllerDlg
ControlCount=28
Control1=IDC_BTN_COMM,button,1342177291
Control2=IDC_BTN_SET,button,1342177291
Control3=IDC_EDIT_CAMERA,edit,1342251137
Control4=IDC_EDIT_POS,edit,1350639745
Control5=IDC_BTN_CALLPOS,button,1342177291
Control6=IDC_BTN_SETPOS,button,1342177291
Control7=IDC_BTN_DELPOS,button,1342177291
Control8=IDC_BTN_ZOOMTELE,button,1342177291
Control9=IDC_BTN_ZOOMWIDE,button,1342177291
Control10=IDC_BTN_FOCUSNEAR,button,1342177291
Control11=IDC_BTN_FOCUSFAR,button,1342177291
Control12=IDC_BTN_IRISCLOSE,button,1342177291
Control13=IDC_BTN_IRISOPEN,button,1342177291
Control14=IDC_BTN_MIN,button,1342177291
Control15=IDC_BTN_CLOSE,button,1342177291
Control16=IDC_S_COMINFO,static,1342308353
Control17=IDC_S_CAMERA,static,1342308354
Control18=IDC_S_POS,static,1342308354
Control19=IDC_S_AD,static,1342308609
Control20=IDC_DIR_LEFTUP,button,1342177291
Control21=IDC_DIR_LEFT,button,1342177291
Control22=IDC_DIR_LEFTDOWN,button,1342177291
Control23=IDC_DIR_UP,button,1342177291
Control24=IDC_DIR_DOWN,button,1342177291
Control25=IDC_DIR_RIGHT,button,1342177291
Control26=IDC_DIR_RIGHTUP,button,1342177291
Control27=IDC_DIR_RIGHTDOWN,button,1342177291
Control28=IDC_DIR_AUTO,button,1342177291

[CLS:CExButton]
Type=0
HeaderFile=cexbutton.h
ImplementationFile=cexbutton.cpp
BaseClass=CButton
LastObject=CExButton

[CLS:CExLabel]
Type=0
HeaderFile=CExLabel.h
ImplementationFile=CExLabel.cpp
BaseClass=CStatic
Filter=W
VirtualFilter=WC

[CLS:CBaseButton]
Type=0
HeaderFile=CBaseButton.h
ImplementationFile=CBaseButton.cpp
BaseClass=CButton
Filter=W
VirtualFilter=BWC
LastObject=CBaseButton

[CLS:CImgButton]
Type=0
HeaderFile=CImgButton.h
ImplementationFile=CImgButton.cpp
BaseClass=CButton
Filter=W
VirtualFilter=BWC
LastObject=CImgButton

[CLS:CDownUpButton]
Type=0
HeaderFile=CDownUpButton.h
ImplementationFile=CDownUpButton.cpp
BaseClass=CButton
Filter=W
VirtualFilter=BWC
LastObject=CDownUpButton

[DLG:IDD_DLG_COVER]
Type=1
Class=CDlgCover
ControlCount=1
Control1=IDC_S_COVER,static,1342308353

[CLS:CDlgCover]
Type=0
HeaderFile=CDlgCover.h
ImplementationFile=CDlgCover.cpp
BaseClass=CDialog
Filter=W
VirtualFilter=dWC
LastObject=IDC_S_COVER

[DLG:IDD_DLG_SET]
Type=1
Class=CDlgSet
ControlCount=17
Control1=IDC_CB_PORT,combobox,1344339971
Control2=IDC_CB_RATE,combobox,1344339971
Control3=IDC_CB_PROTOCOL,combobox,1344339971
Control4=IDC_SLIDER_LR,msctls_trackbar32,1342242840
Control5=IDC_SLIDER_UD,msctls_trackbar32,1342242841
Control6=IDOK,button,1342242817
Control7=IDCANCEL,button,1342242816
Control8=IDC_STATIC,button,1342177287
Control9=IDC_STATIC,static,1342308354
Control10=IDC_STATIC,static,1342308354
Control11=IDC_STATIC,static,1342308354
Control12=IDC_STATIC,button,1342177287
Control13=IDC_STATIC,static,1342308354
Control14=IDC_STATIC,static,1342308354
Control15=IDC_S_SPEED_LR,static,1342308353
Control16=IDC_S_SPEED_UD,static,1342308353
Control17=IDC_S_STUDIO,static,1342308608

[CLS:CDlgSet]
Type=0
HeaderFile=CDlgSet.h
ImplementationFile=CDlgSet.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CDlgSet

[CLS:CLinkLabel]
Type=0
HeaderFile=CLinkLabel.h
ImplementationFile=CLinkLabel.cpp
BaseClass=CStatic
Filter=W
VirtualFilter=WC
LastObject=CLinkLabel

[CLS:CExEdit]
Type=0
HeaderFile=CExEdit.h
ImplementationFile=CExEdit.cpp
BaseClass=CEdit
Filter=W
VirtualFilter=WC
LastObject=CExEdit

