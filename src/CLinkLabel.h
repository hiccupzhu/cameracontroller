#if !defined(AFX_CLINKLABEL_H__AF187E74_99DF_46B6_9DE5_DC8FECCA82D4__INCLUDED_)
#define AFX_CLINKLABEL_H__AF187E74_99DF_46B6_9DE5_DC8FECCA82D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CLinkLabel.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLinkLabel window

class CLinkLabel : public CStatic
{
// Construction
public:
	CLinkLabel();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLinkLabel)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	CString GetUrl();
	VOID SetTextColor(COLORREF color);
	VOID SetURL(CString url);
	virtual ~CLinkLabel();

	// Generated message map functions
protected:
	COLORREF m_nTextColor;
	CString m_szUrl;
	//{{AFX_MSG(CLinkLabel)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnClicked();

	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLINKLABEL_H__AF187E74_99DF_46B6_9DE5_DC8FECCA82D4__INCLUDED_)
