// CameraAction.cpp: implementation of the CCameraAction class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CameraController.h"
#include "CameraAction.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CameraAction::CameraAction()
{
	//BYTE byDbytes[] = {0xFF, 0, 0, 0, 0, 0, 0,	0};	//plus one more byte for convernice
	//BYTE byPbytes[] = {0xA0, 0, 0, 0, 0, 0, 0xAF, 0};

	//init Pelco-D protocol bytes
	for(int i=0; i<PELCO_P_SIZE; i++) {
		pDbytes[i] = 0;
		pPbytes[i] = 0;

	}
	pDbytes[0] = 0xFF;
	pPbytes[0] = 0xA0;

	pPbytes[6] = 0xAF;

	//select D protocol default;
	this->SetPelcoD();

	//set 0 addr default;
	this->SetAddress(1);
}

CameraAction::~CameraAction()
{
}
VOID CameraAction::SetPelcoD()
{
	this->byPelco = 0;
}
VOID CameraAction::SetPelcoP()
{
	this->byPelco = 1;
}
BOOL CameraAction::IsPelcoD()
{
	return this->byPelco == 0;
}
BOOL CameraAction::IsPelcoP()
{
	return this->byPelco == 1;
}
VOID CameraAction::SetAddress(BYTE addr)
{
	this->byCurAddr = addr;
}

BYTE CameraAction::GetBytesSize()
{
	return this->IsPelcoD() ? PELCO_D_SIZE : PELCO_P_SIZE;
}

VOID CameraAction::Prepare(BYTE *pByte)
{
	memcpy(pByte, this->IsPelcoD() ? this->pDbytes : this->pPbytes,
		PELCO_P_SIZE);
	
	*(pByte + 1) = this->byCurAddr;
}

VOID CameraAction::CalcCheckByte(BYTE * pByte)
{
	if(this->IsPelcoD())
	{
		int tmp = 0;
		for(int i=1; i<=5; i++)
		{
			tmp += *(pByte + i);
		}
		*(pByte + PELCO_D_SIZE - 1) = (BYTE)(tmp % 256);
	}
	else
	{
		BYTE tmp = *(pByte + 1);
		for(int i=2; i<=5; i++)
		{
			tmp ^= *(pByte + i);
		}
		*(pByte + PELCO_P_SIZE - 1) = tmp;
	}
}
VOID CameraAction::Stop(BYTE * pByte)
{
	this->Prepare(pByte);

	this->CalcCheckByte(pByte);
}
VOID CameraAction::UpDown(BYTE * pByte, BYTE speed, BOOL bUp)
{
	if(speed > 0x3F)
	{
		speed = 0x3F;
	}

	this->Prepare(pByte);
	
	BYTE byAction = bUp ? 0x08 : 0x10;

	//set up action
	*(pByte + 3) = byAction;

	//set speed
	*(pByte + 5) = speed;

	this->CalcCheckByte(pByte);

}
VOID CameraAction::ToUp(BYTE * pByte, BYTE speed, BOOL b)
{
	if(!b) return;
	this->UpDown(pByte, speed, TRUE);
}

VOID CameraAction::ToDown(BYTE * pByte, BYTE speed, BOOL b)
{
	if(!b) return;
	this->UpDown(pByte, speed, FALSE);

}

VOID CameraAction::LeftRight(BYTE * pByte, BYTE speed, BOOL bLeft)
{
	if(speed > 0x3F)
	{
		speed = this->IsPelcoD() ? 0xFF : 0x40;
	}

	this->Prepare(pByte);
	
	BYTE byAction = bLeft ? 0x04 : 0x02;

	//set up action
	*(pByte + 3) = byAction;

	//set speed
	*(pByte + 4) = speed;

	this->CalcCheckByte(pByte);
}

VOID CameraAction::ToLeft(BYTE * pByte, BYTE speed, BOOL b)
{
	if(!b) return;
	this->LeftRight(pByte, speed, TRUE);
}
VOID CameraAction::ToRight(BYTE * pByte, BYTE speed, BOOL b)
{
	if(!b) return;
	this->LeftRight(pByte, speed, FALSE);
}
VOID CameraAction::ToLeftUp(BYTE *pByte, BYTE speedlr, BYTE speedud, BOOL b)
{
	if(!b) return;
	speedlr = (speedlr>0x3F) ? 0x3F : speedlr;
	speedud = (speedud>0x3F) ? 0x3F : speedud;
	
	this->ToLeft(pByte, speedlr);

	BYTE tmpByte[PELCO_P_SIZE];
	this->ToUp(tmpByte, speedud);

	for(BYTE i=0; i<PELCO_P_SIZE; i++)
	{
		*(pByte + i) |= tmpByte[i];
	}

	this->CalcCheckByte(pByte);
}

VOID CameraAction::ToLeftDown(BYTE *pByte, BYTE speedlr, BYTE speedud, BOOL b)
{
	if(!b) return;
	speedlr = (speedlr>0x3F) ? 0x3F : speedlr;
	speedud = (speedud>0x3F) ? 0x3F : speedud;
	
	this->ToLeft(pByte, speedlr);

	BYTE tmpByte[PELCO_P_SIZE];

	this->ToDown(tmpByte, speedud);

	for(BYTE i=0; i<PELCO_P_SIZE; i++)
	{
		*(pByte + i) |= tmpByte[i];
	}

	this->CalcCheckByte(pByte);
}

VOID CameraAction::ToRightUp(BYTE *pByte, BYTE speedlr, BYTE speedud, BOOL b)
{
	if(!b) return;
	speedlr = (speedlr>0x3F) ? 0x3F : speedlr;
	speedud = (speedud>0x3F) ? 0x3F : speedud;
	
	this->ToRight(pByte, speedlr);

	BYTE tmpByte[PELCO_P_SIZE];

	this->ToUp(tmpByte, speedud);

	for(BYTE i=0; i<PELCO_P_SIZE; i++)
	{
		*(pByte + i) |= tmpByte[i];
	}

	this->CalcCheckByte(pByte);
}
VOID CameraAction::ToRightDown(BYTE *pByte, BYTE speedlr, BYTE speedud, BOOL b)
{
	if(!b) return;
	speedlr = (speedlr>0x3F) ? 0x3F : speedlr;
	speedud = (speedud>0x3F) ? 0x3F : speedud;
	
	this->ToRight(pByte, speedlr);

	BYTE tmpByte[PELCO_P_SIZE];

	this->ToDown(tmpByte, speedud);

	for(BYTE i=0; i<PELCO_P_SIZE; i++)
	{
		*(pByte + i) |= tmpByte[i];
	}

	this->CalcCheckByte(pByte);
}
VOID CameraAction::AutoStart(BYTE *pByte, BYTE speedlr, BYTE speedud, BOOL b)
{
	if(!b) return;
	speedlr = (speedlr>0x3F) ? 0x3F : speedlr;
	speedud = (speedud>0x3F) ? 0x3F : speedud;

	this->Prepare(pByte);
	
	//set up action
	*(pByte + 2) = this->IsPelcoD()?0x98:0x20;

	//set speed
	*(pByte + 4) = speedlr;
	*(pByte + 5) = speedud;

	this->CalcCheckByte(pByte);
}

VOID CameraAction::ZoomWide(BYTE *pByte, BOOL b)
{
	if(!b) return;
	this->Prepare(pByte);
	*(pByte + 3) = 0x40;
	this->CalcCheckByte(pByte);
}

VOID CameraAction::ZoomTele(BYTE *pByte, BOOL b)
{
	if(!b) return;
	this->Prepare(pByte);
	*(pByte + 3) = 0x20;
	this->CalcCheckByte(pByte);
}

VOID CameraAction::FocusFar(BYTE *pByte, BOOL b)
{
	if(!b) return;
	this->Prepare(pByte);
	*(pByte + 2) = this->IsPelcoD()?0x00:0x01;
	*(pByte + 3) = this->IsPelcoD()?0x80:0x00;
	this->CalcCheckByte(pByte);
}

VOID CameraAction::FocusNear(BYTE *pByte, BOOL b)
{
	if(!b) return;
	this->Prepare(pByte);
	*(pByte + 2) = this->IsPelcoD()?0x01:0x02;
	this->CalcCheckByte(pByte);
}

VOID CameraAction::IrisClose(BYTE *pByte, BOOL b)
{
	if(!b) return;
	this->Prepare(pByte);
	*(pByte + 2) = this->IsPelcoD()?0x40:0x80;
	this->CalcCheckByte(pByte);
}

VOID CameraAction::IrisOpen(BYTE *pByte, BOOL b)
{
	if(!b) return;
	this->Prepare(pByte);
	*(pByte + 2) = this->IsPelcoD()?0x20:0x40;
	this->CalcCheckByte(pByte);
}

VOID CameraAction::SetPreset(BYTE *pByte, BYTE pos)
{
	this->PreparePreset(pByte, pos);
	*(pByte + 3) = 0x03;
	this->CalcCheckByte(pByte);

}
VOID CameraAction::DelPreset(BYTE *pByte, BYTE pos)
{
	this->PreparePreset(pByte, pos);
	*(pByte + 3) = 0x05;
	this->CalcCheckByte(pByte);
}

VOID CameraAction::CallPreset(BYTE *pByte, BYTE pos)
{
	this->PreparePreset(pByte, pos);
	*(pByte + 3) = 0x07;
	this->CalcCheckByte(pByte);
}

VOID CameraAction::PreparePreset(BYTE *pByte, BYTE pos)
{
	if(pos<1) pos = 1;
	if(pos>0xff) pos = 0xff;
	if(this->IsPelcoD() && pos>0x20) pos = 0x20;
	this->Prepare(pByte);
	
	*(pByte + 5) = pos;
}

int CameraAction::GetCorrectPos(int pos)
{
	if(pos<1) pos = 1;
	if(pos>0xff) pos = 0xff;
	if(this->IsPelcoD() && pos>0x20) pos = 0x20;
	return pos;
}
