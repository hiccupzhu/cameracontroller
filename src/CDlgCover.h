#if !defined(AFX_CDLGCOVER_H__06ECA3A6_F514_4693_8966_FF5C77FC70EE__INCLUDED_)
#define AFX_CDLGCOVER_H__06ECA3A6_F514_4693_8966_FF5C77FC70EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CDlgCover.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgCover dialog

#include "CExLabel.h"

class CDlgCover : public CDialog
{
// Construction
public:
	CDlgCover(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgCover)
	enum { IDD = IDD_DLG_COVER };
	CExLabel	m_sCover;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgCover)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgCover)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CDLGCOVER_H__06ECA3A6_F514_4693_8966_FF5C77FC70EE__INCLUDED_)
