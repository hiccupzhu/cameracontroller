// CDlgSet.cpp : implementation file
//

#include "stdafx.h"
#include "CameraController.h"
#include "CDlgSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgSet dialog


CDlgSet::CDlgSet(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSet::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgSet)
	m_nPort = -1;
	m_nProtocol = -1;
	m_nRate = -1;
	m_szSpeedLr = _T("");
	m_szSpeedUd = _T("");
	//}}AFX_DATA_INIT
}


void CDlgSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSet)
	DDX_Control(pDX, IDC_S_STUDIO, m_sStudio);
	DDX_Control(pDX, IDC_SLIDER_UD, m_sliderUd);
	DDX_Control(pDX, IDC_SLIDER_LR, m_sliderLr);
	DDX_CBIndex(pDX, IDC_CB_PORT, m_nPort);
	DDX_CBIndex(pDX, IDC_CB_PROTOCOL, m_nProtocol);
	DDX_CBIndex(pDX, IDC_CB_RATE, m_nRate);
	DDX_Text(pDX, IDC_S_SPEED_LR, m_szSpeedLr);
	DDV_MaxChars(pDX, m_szSpeedLr, 4);
	DDX_Text(pDX, IDC_S_SPEED_UD, m_szSpeedUd);
	DDV_MaxChars(pDX, m_szSpeedUd, 4);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgSet, CDialog)
	//{{AFX_MSG_MAP(CDlgSet)
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgSet message handlers

void CDlgSet::OnOK() 
{
	
	CDialog::OnOK();
}

void CDlgSet::OnCancel() 
{
	
	CDialog::OnCancel();
}

void CDlgSet::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CSliderCtrl * p = (CSliderCtrl*)pScrollBar;
	if(p->GetDlgCtrlID()==IDC_SLIDER_LR) {
		this->m_szSpeedLr.Format("%d",p->GetPos());
	}else if(p->GetDlgCtrlID()==IDC_SLIDER_UD) {
		this->m_szSpeedUd.Format("%d",p->GetPos());
	}
	this->UpdateData(0);
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

BOOL CDlgSet::OnInitDialog() 
{
	CDialog::OnInitDialog();
	this->EnableToolTips();
	m_tt.Create(this);
	m_tt.Activate(TRUE);

	m_sliderLr.SetRange(0,63,TRUE);
	m_sliderUd.SetRange(0,63,TRUE);
	m_sliderLr.SetPos(atoi(this->m_szSpeedLr));
	m_sliderUd.SetPos(atoi(this->m_szSpeedUd));

	this->m_sStudio.SetURL("http://xygroup.blog.sohu.com");
	m_tt.AddTool(&m_sStudio, m_sStudio.GetUrl());

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CDlgSet::PreTranslateMessage(MSG* pMsg) 
{
	m_tt.RelayEvent(pMsg);	
	return CDialog::PreTranslateMessage(pMsg);
}
