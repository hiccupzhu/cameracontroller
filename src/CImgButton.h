#if !defined(AFX_EXBUTTON_H__9A868423_DA8D_4483_B7AC_DF83688A837E__INCLUDED_)
#define AFX_EXBUTTON_H__9A868423_DA8D_4483_B7AC_DF83688A837E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//

#include "CBaseButton.h"

#include <afxmt.h>

/////////////////////////////////////////////////////////////////////////////
// CImgButton window

class CImgButton : public CBaseButton
{
protected:
	CBitmap m_bitmap;

	COLORREF m_nTransColor;

	INT m_width;
	INT m_height;

// Construction
public:
	CImgButton();
	
	VOID SetBitmaps(UINT nIDBitmap, UINT align = 0x0F, COLORREF transColor=RGB(255,255,255));

	//align type
	static UINT ALIGN_TOP;
	static UINT ALIGN_LEFT;
	static UINT ALIGN_RIGHT;
	static UINT ALIGN_BOTTOM;
	static UINT ALIGN_CENTER;

	//state
	static UINT STATE_DEFAULT;
	static UINT STATE_HOVER;
	static UINT STATE_CLICKED;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImgButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	VOID SetState(UINT state = STATE_DEFAULT);
	VOID SetTextColor(COLORREF c);
	BOOL GetRgnReady();
	static LRESULT CALLBACK MakeRgn(CImgButton * pBtn);
	virtual ~CImgButton();

	// Generated message map functions
protected:
	UINT m_nState;
	COLORREF m_nSetTransColor;
	COLORREF m_nTextColor;
	//{{AFX_MSG(CImgButton)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	DWORD m_dwMakeRgnThreadID;
	BOOL m_bRgnReady;
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXBUTTON_H__9A868423_DA8D_4483_B7AC_DF83688A837E__INCLUDED_)
