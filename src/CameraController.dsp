# Microsoft Developer Studio Project File - Name="CameraController" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CameraController - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CameraController.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CameraController.mak" CFG="CameraController - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CameraController - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CameraController - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CameraController - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x804 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x804 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "CameraController - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x804 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x804 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "CameraController - Win32 Release"
# Name "CameraController - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\CameraAction.cpp
# End Source File
# Begin Source File

SOURCE=.\CameraController.cpp
# End Source File
# Begin Source File

SOURCE=.\CameraController.rc
# End Source File
# Begin Source File

SOURCE=.\CameraControllerDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CBaseButton.cpp
# End Source File
# Begin Source File

SOURCE=.\CDlgCover.cpp
# End Source File
# Begin Source File

SOURCE=.\CDlgSet.cpp
# End Source File
# Begin Source File

SOURCE=.\CExEdit.cpp
# End Source File
# Begin Source File

SOURCE=.\CExLabel.cpp
# End Source File
# Begin Source File

SOURCE=.\CImgButton.cpp
# End Source File
# Begin Source File

SOURCE=.\CLinkLabel.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\CameraAction.h
# End Source File
# Begin Source File

SOURCE=.\CameraController.h
# End Source File
# Begin Source File

SOURCE=.\CameraControllerDlg.h
# End Source File
# Begin Source File

SOURCE=.\CBaseButton.h
# End Source File
# Begin Source File

SOURCE=.\CDlgCover.h
# End Source File
# Begin Source File

SOURCE=.\CDlgSet.h
# End Source File
# Begin Source File

SOURCE=.\CExEdit.h
# End Source File
# Begin Source File

SOURCE=.\CExLabel.h
# End Source File
# Begin Source File

SOURCE=.\CImgButton.h
# End Source File
# Begin Source File

SOURCE=.\CLinkLabel.h
# End Source File
# Begin Source File

SOURCE=.\CnComm.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bg.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bin1.bin
# End Source File
# Begin Source File

SOURCE=.\res\btnlong.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CameraController.ico
# End Source File
# Begin Source File

SOURCE=.\res\CameraController.rc2
# End Source File
# Begin Source File

SOURCE=.\res\close.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dir.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dir_auto.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dir_down.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dir_left.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dir_leftdown.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dir_leftup.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dir_righ.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dir_rightdown.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dir_rightup.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dir_up.bmp
# End Source File
# Begin Source File

SOURCE=.\res\gomax.bmp
# End Source File
# Begin Source File

SOURCE=.\res\gomin.bmp
# End Source File
# Begin Source File

SOURCE=.\res\harrow.cur
# End Source File
# Begin Source File

SOURCE=.\res\idr_main.ico
# End Source File
# Begin Source File

SOURCE=.\res\main_close.ico
# End Source File
# Begin Source File

SOURCE=.\res\main_open.ico
# End Source File
# Begin Source File

SOURCE=.\res\min.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
