// CameraController.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CameraController.h"
#include "CameraControllerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCameraControllerApp

BEGIN_MESSAGE_MAP(CCameraControllerApp, CWinApp)
	//{{AFX_MSG_MAP(CCameraControllerApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCameraControllerApp construction

CCameraControllerApp::CCameraControllerApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCameraControllerApp object

CCameraControllerApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCameraControllerApp initialization

BOOL CCameraControllerApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CCameraControllerDlg dlg;

	//cover window
	dlg.m_dlgCover.SetParent(&dlg);
	dlg.m_dlgCover.Create(IDD_DLG_COVER, &dlg);
	dlg.m_dlgCover.ShowWindow(SW_SHOW);
	dlg.m_dlgCover.UpdateWindow();

	//main window
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
