#if !defined(AFX_CDLGSET_H__1BE73FCB_B8FA_45FF_A738_6C2E8001E9BE__INCLUDED_)
#define AFX_CDLGSET_H__1BE73FCB_B8FA_45FF_A738_6C2E8001E9BE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CDlgSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgSet dialog
#include "CLinkLabel.h"

class CDlgSet : public CDialog
{
// Construction
public:
	CToolTipCtrl m_tt;
	CDlgSet(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgSet)
	enum { IDD = IDD_DLG_SET };
	CLinkLabel	m_sStudio;
	CSliderCtrl	m_sliderUd;
	CSliderCtrl	m_sliderLr;
	int		m_nPort;
	int		m_nProtocol;
	int		m_nRate;
	CString	m_szSpeedLr;
	CString	m_szSpeedUd;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgSet)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgSet)
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CDLGSET_H__1BE73FCB_B8FA_45FF_A738_6C2E8001E9BE__INCLUDED_)
