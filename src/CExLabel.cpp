// CExLabel.cpp : implementation file
//

#include "stdafx.h"
#include "CameraController.h"
#include "CExLabel.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExLabel

CExLabel::CExLabel()
{
	this->m_textColor = RGB(255,255,255);
}

CExLabel::~CExLabel()
{
}


BEGIN_MESSAGE_MAP(CExLabel, CStatic)
	//{{AFX_MSG_MAP(CExLabel)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExLabel message handlers

HBRUSH CExLabel::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	// TODO: Change any attributes of the DC here
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(this->m_textColor);
	
	return (HBRUSH)::GetStockObject(NULL_BRUSH);;
}


VOID CExLabel::SetTextColor(COLORREF color)
{
	this->m_textColor = color;
	this->Invalidate();
}

BOOL CExLabel::OnEraseBkgnd(CDC* pDC) 
{
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(this->m_textColor);
	return CStatic::OnEraseBkgnd(pDC);
}

