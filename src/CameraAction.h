// CameraAction.h: interface for the CameraAction class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CAMERAACTION_H__958F7A25_76EF_4490_89D9_1B18D96E59E0__INCLUDED_)
#define AFX_CAMERAACTION_H__958F7A25_76EF_4490_89D9_1B18D96E59E0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

const int PELCO_D_SIZE = 7;
const int PELCO_P_SIZE = 8;

class CameraAction  
{
public:
	int GetCorrectPos(int pos);
	VOID DelPreset(BYTE *pByte, BYTE pos);
	VOID CallPreset(BYTE *pByte, BYTE pos);
	VOID SetPreset(BYTE * pByte, BYTE pos);
	CameraAction();
	
	//设置协议
	VOID SetPelcoP();
	VOID SetPelcoD();

	//设置地址码
	VOID SetAddress(BYTE addr);

	//查询协议
	BOOL IsPelcoP();
	BOOL IsPelcoD();

	BYTE GetBytesSize();

	//finish code
	VOID Stop(BYTE * pByte);

	VOID ToUp(BYTE * pByte, BYTE speed, BOOL b = TRUE);
	VOID ToDown(BYTE * pByte, BYTE speed, BOOL b = TRUE);
	VOID ToLeft(BYTE * pByte, BYTE speed, BOOL b = TRUE);
	VOID ToRight(BYTE * pByte, BYTE speed, BOOL b = TRUE);
	
	VOID ToLeftUp(BYTE * pByte, BYTE speedlr, BYTE speedud, BOOL b = TRUE);
	VOID ToLeftDown(BYTE * pByte, BYTE speedlr, BYTE speedud, BOOL b = TRUE);
	VOID ToRightUp(BYTE * pByte, BYTE speedlr, BYTE speedud, BOOL b = TRUE);
	VOID ToRightDown(BYTE * pByte, BYTE speedlr, BYTE speedud, BOOL b = TRUE);

	VOID ZoomWide(BYTE * pByte, BOOL b = TRUE);
	VOID ZoomTele(BYTE * pByte, BOOL b = TRUE);

	VOID FocusFar(BYTE *pByte, BOOL b = TRUE);
	VOID FocusNear(BYTE *pByte, BOOL b = TRUE);

	VOID IrisOpen(BYTE *pByte, BOOL b = TRUE);
	VOID IrisClose(BYTE *pByte, BOOL b = TRUE);

	VOID AutoStart(BYTE *pByte, BYTE speedlr, BYTE speedud, BOOL b = TRUE);

	virtual ~CameraAction();

private:
	BYTE byPelco;
	BYTE byCurAddr;
	BYTE bySpeed;


	BYTE pDbytes[8];
	BYTE pPbytes[8];

	VOID UpDown(BYTE * pByte, BYTE speed, BOOL bUp);
	VOID LeftRight(BYTE * pByte, BYTE speed, BOOL bLeft);

	VOID CalcCheckByte(BYTE * pByte);
	VOID PreparePreset(BYTE *pByte, BYTE pos);
	VOID Prepare(BYTE *pByte);
};

#endif // !defined(AFX_CAMERAACTION_H__958F7A25_76EF_4490_89D9_1B18D96E59E0__INCLUDED_)
