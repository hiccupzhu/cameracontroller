//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CameraController.rc
//
#define IDD_CameraController_DIALOG     102
#define IDS_TITLE                       102
#define IDR_MAINFRAME                   128
#define IDR_MAIN_OPEN                   129
#define IDR_MAIN_CLOSE                  130
#define IDB_BG                          144
#define IDB_MIN                         146
#define IDB_CLOSE                       148
#define IDB_BTNLONG                     149
#define IDB_GOMIN                       150
#define IDB_GOMAX                       151
#define IDB_DIR_LEFT                    152
#define IDB_DIR_LEFTUP                  153
#define IDB_DIR_LEFTDOWN                154
#define IDB_DIR_UP                      155
#define IDB_DIR_DOWN                    156
#define IDB_DIR_RIGHT                   157
#define IDB_DIR_RIGHTUP                 158
#define IDB_DIR_RIGHTDOWN               159
#define IDB_DIR_AUTO                    160
#define IDD_DLG_COVER                   161
#define IDD_DLG_SET                     162
#define IDC_HAND                        164
#define IDC_BTN_MIN                     1000
#define IDC_BTN_CLOSE                   1001
#define IDC_BTN_COMM                    1032
#define IDC_BTN_SET                     1033
#define IDC_EDIT_CAMERA                 1034
#define IDC_EDIT_POS                    1035
#define IDC_BTN_CALLPOS                 1036
#define IDC_BTN_SETPOS                  1037
#define IDC_BUTTON7                     1038
#define IDC_BTN_DELPOS                  1038
#define IDC_BTN_ZOOMTELE                1040
#define IDC_BTN_ZOOMWIDE                1041
#define IDC_BTN_FOCUSNEAR               1042
#define IDC_BTN_FOCUSFAR                1043
#define IDC_BTN_IRISCLOSE               1044
#define IDC_BTN_IRISOPEN                1045
#define IDC_S_COMINFO                   1051
#define IDC_S_AD                        1053
#define IDC_S_CAMERA                    1054
#define IDC_S_POS                       1055
#define IDC_DIR_LEFTUP                  1056
#define IDC_DIR_LEFT                    1057
#define IDC_DIR_LEFTDOWN                1058
#define IDC_DIR_UP                      1059
#define IDC_DIR_DOWN                    1060
#define IDC_DIR_RIGHT                   1061
#define IDC_DIR_RIGHTUP                 1062
#define IDC_DIR_RIGHTDOWN               1063
#define IDC_DIR_AUTO                    1064
#define IDC_S_COVER                     1065
#define IDC_CB_PORT                     1066
#define IDC_CB_RATE                     1067
#define IDC_CB_PROTOCOL                 1068
#define IDC_SLIDER_LR                   1069
#define IDC_S_SPEED_LR                  1070
#define IDC_SLIDER_UD                   1071
#define IDC_S_SPEED_UD                  1072
#define IDC_SCROLLBAR1                  1073
#define IDC_S_STUDIO                    1074

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        165
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1075
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
