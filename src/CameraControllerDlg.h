// CameraControllerDlg.h : header file
//

#if !defined(AFX_CameraControllerDLG_H__AB4A6E90_1FE1_41E1_B600_45F8B14FA031__INCLUDED_)
#define AFX_CameraControllerDLG_H__AB4A6E90_1FE1_41E1_B600_45F8B14FA031__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CImgButton.h"
#include "CExLabel.h"
#include "CLinkLabel.h"
#include "CDlgCover.h"
#include "CExEdit.h"
#include "CnComm.h"
#include "CameraAction.h"

/////////////////////////////////////////////////////////////////////////////
// CCameraControllerDlg dialog

class CCameraControllerDlg : public CDialog
{
// Construction
public:
	VOID StopCameraAction();
	CnComm com;
	CameraAction action;

	CString m_iniSec;
	CString m_ini_port;
	CString m_ini_rate;
	CString m_ini_protocol;
	CString m_ini_speedlr;
	CString m_ini_speedud;
	CString m_ini_camera;
	CString m_ini_pos;

	CToolTipCtrl m_tt;
	VOID SetCommInfo();
	INT m_nSpeedUd;		//0-63
	INT m_nSpeedLr;		//0-63
	INT m_nProtocol;	//0: pelco-d, 1: pelco-p
	INT m_nRate;		//based on 0
	INT m_nRates[5];	//2400, 4800, 9600, 14400, 19200
	INT m_nPort;		//1,2,3,4

	CDlgCover m_dlgCover;
	CCameraControllerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CCameraControllerDlg)
	enum { IDD = IDD_CameraController_DIALOG };
	CExEdit	m_editPos;
	CExEdit	m_editCamera;
	CImgButton	m_btnDirAuto;
	CImgButton	m_btnDirDown;
	CImgButton	m_btnDirLeftDown;
	CImgButton	m_btnDirRight;
	CImgButton	m_btnDirRightDown;
	CImgButton	m_btnDirRightUp;
	CImgButton	m_btnDirUp;
	CImgButton	m_btnDirLeftUp;
	CImgButton	m_btnDirLeft;
	CImgButton	m_btnIrisOpen;
	CImgButton	m_btnIrisClose;
	CImgButton	m_btnFocusNear;
	CImgButton	m_btnFocusFar;
	CImgButton	m_btnZoomWide;
	CImgButton	m_btnZoomTele;
	CImgButton	m_btnSetPos;
	CImgButton	m_btnDelPos;
	CImgButton	m_btnCallPos;
	CImgButton	m_btnSet;
	CImgButton	m_btnComm;
	CImgButton	m_btnClose;
	CImgButton	m_btnMin;
	CExLabel	m_sPos;
	CExLabel	m_sCamera;
	CLinkLabel	m_sAd;
	CExLabel	m_sCominfo;
	int		m_nCamera;
	int		m_nPos;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCameraControllerDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL m_bAutoed;
	BOOL m_bOpend;
	HICON m_hIcon;
	CString m_szTitle;

	// Generated message map functions
	//{{AFX_MSG(CCameraControllerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnClose();
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnBtnClose();
	afx_msg void OnBtnMin();
	afx_msg void OnBtnComm();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnBtnSet();
	afx_msg void OnChangeEditCamera();
	afx_msg void OnChangeEditPos();
	afx_msg void OnDirAuto();
	afx_msg void OnBtnCallpos();
	afx_msg void OnBtnSetpos();
	afx_msg void OnBtnDelpos();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	VOID StopLightFlash();
	VOID StartLightFlash();
	UINT m_nLightTimer;
	BOOL m_bFlash;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CameraControllerDLG_H__AB4A6E90_1FE1_41E1_B600_45F8B14FA031__INCLUDED_)
