#if !defined(AFX_CEXLABEL_H__43649192_D2BB_40FA_ADE1_B168D5DC9416__INCLUDED_)
#define AFX_CEXLABEL_H__43649192_D2BB_40FA_ADE1_B168D5DC9416__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CExLabel.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CExLabel window

class CExLabel : public CStatic
{
// Construction
public:
	CExLabel();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExLabel)
	//}}AFX_VIRTUAL

// Implementation
public:
	VOID SetTextColor(COLORREF color);
	virtual ~CExLabel();

	// Generated message map functions
protected:
	//{{AFX_MSG(CExLabel)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	COLORREF m_textColor;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CEXLABEL_H__43649192_D2BB_40FA_ADE1_B168D5DC9416__INCLUDED_)
