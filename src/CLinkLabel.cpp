// CLinkLabel.cpp : implementation file
//

#include "stdafx.h"
#include "CameraController.h"
#include "CLinkLabel.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLinkLabel

CLinkLabel::CLinkLabel()
{
	this->m_nTextColor = RGB(0,0,255);
	this->m_szUrl = "";

}

CLinkLabel::~CLinkLabel()
{
}


BEGIN_MESSAGE_MAP(CLinkLabel, CStatic)
	//{{AFX_MSG_MAP(CLinkLabel)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_ERASEBKGND()
	ON_CONTROL_REFLECT(BN_CLICKED, OnClicked)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLinkLabel message handlers

HBRUSH CLinkLabel::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(this->m_nTextColor);

	CFont * font = this->GetFont();

	LOGFONT logfont;
	font->GetLogFont(&logfont);

	logfont.lfUnderline=TRUE;

	CFont myLogFont; 
	myLogFont.CreateFontIndirect(&logfont);

	pDC->SelectObject(&myLogFont);
	
	return (HBRUSH)::GetStockObject(NULL_BRUSH);;
}


BOOL CLinkLabel::OnEraseBkgnd(CDC* pDC) 
{
	pDC->SetBkMode(TRANSPARENT);
	return CStatic::OnEraseBkgnd(pDC);
}

VOID CLinkLabel::SetURL(CString url)
{
	this->m_szUrl = url;
}

VOID CLinkLabel::SetTextColor(COLORREF color)
{
	this->m_nTextColor = color;
	this->Invalidate();
}

void CLinkLabel::OnClicked() 
{
	if(this->m_szUrl.GetLength()>0) {
		::ShellExecute(NULL,"open",this->m_szUrl,NULL,NULL,SW_MAXIMIZE);
	}
}

CString CLinkLabel::GetUrl()
{
	return this->m_szUrl;
}

BOOL CLinkLabel::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_MOUSEMOVE) {
		if(this->m_szUrl.GetLength()>0) {
			::SetCursor(::AfxGetApp()->LoadCursor(IDC_HAND));
		}else{
			::SetCursor(::LoadCursor(NULL,MAKEINTRESOURCE(IDC_ARROW)));
		}
	}
	
	return CStatic::PreTranslateMessage(pMsg);
}

