// CBaseButton.cpp : implementation file
//

#include "stdafx.h"
#include "CameraController.h"
#include "CBaseButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBaseButton

CBaseButton::CBaseButton()
{
	this->m_bHover = FALSE;
	this->m_nIDTimerHover = 0;
	hRgn = CreateRectRgn(0,0,0,0);
}

CBaseButton::~CBaseButton()
{
	this->KillTimer(this->m_nIDTimerHover);
	::DeleteObject(hRgn);
}


BEGIN_MESSAGE_MAP(CBaseButton, CButton)
	//{{AFX_MSG_MAP(CBaseButton)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBaseButton message handlers

VOID CBaseButton::TransparentBlt( CDC* hdcDest,        // 目标DC
                     int nXOriginDest,   // 目标X偏移
                     int nYOriginDest,   // 目标Y偏移
                     int nWidthDest,     // 目标宽度
                     int nHeightDest,    // 目标高度
                     CDC* hdcSrc,         // 源DC
                     int nXOriginSrc,    // 源X起点
                     int nYOriginSrc,    // 源Y起点
                     int nWidthSrc,      // 源宽度
                     int nHeightSrc,     // 源高度
                     UINT crTransparent  // 透明色,COLORREF类型
                )
{
    CBitmap hImageBMP; 
    hImageBMP.CreateCompatibleBitmap(hdcDest, nWidthDest, nHeightDest);// 创建兼容位图

    CBitmap hMaskBMP;
    hMaskBMP.CreateBitmap(nWidthDest, nHeightDest, 1, 1, NULL);// 创建单色掩码位图

    CDC  hImageDC;
    CDC  hMaskDC;
    hImageDC.CreateCompatibleDC(hdcDest);
    hMaskDC.CreateCompatibleDC(hdcDest);

    CBitmap* hOldImageBMP = hImageDC.SelectObject(&hImageBMP);
    CBitmap* hOldMaskBMP = hMaskDC.SelectObject(&hMaskBMP);
 
    // 将源DC中的位图拷贝到临时DC中
    if (nWidthDest == nWidthSrc && nHeightDest == nHeightSrc)
        hImageDC.BitBlt(0, 0, nWidthDest, nHeightDest, hdcSrc, nXOriginSrc, nYOriginSrc, SRCCOPY);
    else
        hImageDC.StretchBlt(0, 0, nWidthDest, nHeightDest, 
                   hdcSrc, nXOriginSrc, nYOriginSrc, nWidthSrc, nHeightSrc, SRCCOPY);
 
    // 设置透明色
    hImageDC.SetBkColor(crTransparent);
 
    // 生成透明区域为白色，其它区域为黑色的掩码位图
    hMaskDC.BitBlt(0, 0, nWidthDest, nHeightDest, &hImageDC, 0, 0, SRCCOPY);
 
    // 生成透明区域为黑色，其它区域保持不变的位图
    hImageDC.SetBkColor(RGB(0,0,0));
    hImageDC.SetTextColor(crTransparent);
    hImageDC.BitBlt(0, 0, nWidthDest, nHeightDest, &hMaskDC, 0, 0, SRCAND);
 
    // 透明部分保持屏幕不变，其它部分变成黑色
    hdcDest->SetBkColor(crTransparent);
    hdcDest->SetTextColor(RGB(0,0,0));
    hdcDest->BitBlt(nXOriginDest, nYOriginDest, nWidthDest, nHeightDest, &hMaskDC, 0, 0, SRCAND);
 
    // "或"运算,生成最终效果
    hdcDest->BitBlt(nXOriginDest, nYOriginDest, nWidthDest, nHeightDest, &hImageDC, 0, 0, SRCPAINT);
 
    // 清理、恢复 
    hImageDC.SelectObject(hOldImageBMP);
    hImageDC.DeleteDC();

    hMaskDC.SelectObject(hOldMaskBMP);
    hMaskDC.DeleteDC();

    hImageBMP.DeleteObject();
    hMaskBMP.DeleteObject();
}

void CBaseButton::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == this->m_nIDTimerHover)
	{
		CPoint pt;
		GetCursorPos(&pt);
		this->ScreenToClient(&pt);
				
		this->GetWindowRgn(hRgn);

		if(PtInRegion(hRgn, pt.x, pt.y))
		{
			if(this->m_bHover) {
				goto END;
			}else{
				this->m_bHover = TRUE;
				this->Invalidate();
				this->UpdateWindow();
			}
		}else{
			if(!this->m_bHover) {
				goto END;
			}else{
				this->m_bHover = FALSE;
				this->Invalidate();
				this->UpdateWindow();
			}
		}
		
	}
END: CButton::OnTimer(nIDEvent);
}
