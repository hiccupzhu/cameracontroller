
#include "stdafx.h"
#include "CameraController.h"
#include "CImgButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImgButton

UINT CImgButton::ALIGN_LEFT = 0x01;
UINT CImgButton::ALIGN_TOP = 0x02;
UINT CImgButton::ALIGN_RIGHT = 0x04;
UINT CImgButton::ALIGN_BOTTOM = 0x08;
UINT CImgButton::ALIGN_CENTER = 0x0F;

UINT CImgButton::STATE_DEFAULT = 0x01;
UINT CImgButton::STATE_HOVER = 0x02;
UINT CImgButton::STATE_CLICKED = 0x04;

CImgButton::CImgButton()
{
	this->m_bRgnReady = FALSE;
	this->m_nState = STATE_DEFAULT;
	this->m_nTextColor = (RGB(0,0,0));
}

CImgButton::~CImgButton()
{
	this->m_bitmap.DeleteObject();
}


BEGIN_MESSAGE_MAP(CImgButton, CButton)
	//{{AFX_MSG_MAP(CImgButton)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImgButton message handlers

VOID CImgButton::SetBitmaps(UINT nIDBitmap, UINT align, COLORREF transColor)
{
	this->m_nSetTransColor = transColor;

	this->m_bRgnReady = FALSE;
	this->m_bitmap.DeleteObject();
	this->m_bitmap.LoadBitmap(nIDBitmap);

	BITMAP bit;
	this->m_bitmap.GetBitmap(&bit);
	this->m_width = bit.bmWidth / 5;	//default/hover/focus/click/disabled
	this->m_height = bit.bmHeight;

	CDC *pDC = this->GetDC();

	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	memDC.SelectObject(&this->m_bitmap);
	
	this->m_nTransColor = memDC.GetPixel(0,0);
	
	CRect rect;
	this->GetWindowRect(&rect);
	this->GetParent()->ScreenToClient(&rect);
	
	int dx = (rect.Width()-m_width)/2;
	int dy = (rect.Height()-m_height)/2;

	int nleft = rect.left;
	int ntop = rect.top;

	if(align & ALIGN_RIGHT) {
		nleft += dx;
	}
	if(align & ALIGN_BOTTOM) {
		ntop += dy;
	}
	if((align & ALIGN_LEFT) == 0x00) {
		nleft += dx;
	}
	if((align & ALIGN_TOP) == 0x00) {
		ntop += dy;
	}
		
	this->MoveWindow(nleft, ntop,
		nleft+m_width, ntop+m_height);
	
	memDC.DeleteDC();
	this->ReleaseDC(pDC);

	if(this->m_nTransColor == this->m_nSetTransColor) {
		/*
		MakeRgnThreadID = 0;

		CreateThread(NULL,
			0,
			(LPTHREAD_START_ROUTINE)MakeRgn,
			(LPVOID)this,
			0,
			&MakeRgnThreadID);
		*/
		MakeRgn(this);
	}else{
		CRgn wRgn;
		wRgn.CreateRectRgn(0,0,m_width,m_height);
		this->SetWindowRgn((HRGN)wRgn.m_hObject, TRUE);
		wRgn.DeleteObject();
	}

	this->m_nIDTimerHover = (UINT)this->GetSafeHwnd();

	this->KillTimer(this->m_nIDTimerHover);
	this->SetTimer(this->m_nIDTimerHover, 100, NULL);
}

void CImgButton::DrawItem(LPDRAWITEMSTRUCT lpDis) 
{	

	CDC *pDC = CDC::FromHandle(lpDis->hDC);

	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	memDC.SelectObject(&this->m_bitmap);

	UINT nState = lpDis->itemState;

	int nsPos = 0;

	if(this->m_nState == STATE_CLICKED 
			|| nState & ODS_SELECTED) {
		nsPos = m_width + m_width + m_width;

	} else if(nState & ODS_DISABLED) {
		nsPos = m_width + m_width + m_width + m_width;

	} else if(this->m_nState == STATE_HOVER
			|| this->m_bHover) {
		nsPos = m_width;

	} else if(nState & ODS_FOCUS) {
		lpDis->itemAction = ODA_FOCUS;

		nsPos = m_width + m_width;

	}
	
	if(m_nTransColor==m_nSetTransColor) {
		this->TransparentBlt(pDC, 0, 0, m_width, m_height, 
			&memDC, nsPos, 0, m_width, m_height, m_nTransColor);
	}else{
		pDC->BitBlt(0,0,m_width,m_height,&memDC,nsPos,0,SRCCOPY);
	}

	memDC.DeleteDC();

	CRect rect(0,0,m_width,m_height);
	CString s;
	this->GetWindowText(s);
	pDC->SetTextColor(this->m_nTextColor);
	pDC->SetBkMode(TRANSPARENT);
	pDC->DrawText(s,&rect,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	this->ReleaseDC(pDC);
}

HBRUSH CImgButton::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	pDC->SetBkMode(TRANSPARENT);
	
	return (HBRUSH)::GetStockObject(NULL_BRUSH);;
}

LRESULT CALLBACK CImgButton::MakeRgn(CImgButton *pBtn)
{
	CDC *pDC = pBtn->GetDC();

	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	memDC.SelectObject(&pBtn->m_bitmap);
	
	CRgn wRgn;
	wRgn.CreateRectRgn(0,0,pBtn->m_width,pBtn->m_height);

	CRgn tmpRgn;
	
	for(int x=0; x<pBtn->m_width; x++) {
		for(int y=0; y<pBtn->m_height; y++) {
			COLORREF c = memDC.GetPixel(x,y);
			if(pBtn->m_nSetTransColor == c) {
				tmpRgn.CreateRectRgn(x,y,x+1,y+1);
				wRgn.CombineRgn(&wRgn, &tmpRgn, RGN_XOR);
				tmpRgn.DeleteObject();
			}
		}
	}

	tmpRgn.DeleteObject();

	::SetWindowRgn(pBtn->GetSafeHwnd(), (HRGN)wRgn.m_hObject, TRUE);
	wRgn.DeleteObject();

	memDC.DeleteDC();
	pBtn->ReleaseDC(pDC);

	pBtn->m_bRgnReady = TRUE;

	pBtn->Invalidate();
	
	return 0;
}

BOOL CImgButton::GetRgnReady()
{
	return this->m_bRgnReady;
}

void CImgButton::OnTimer(UINT nIDEvent) 
{	
	CBaseButton::OnTimer(nIDEvent);
}

VOID CImgButton::SetTextColor(COLORREF c)
{
	this->m_nTextColor = c;
	this->Invalidate();
}


VOID CImgButton::SetState(UINT state)
{
	this->m_nState = state;
	this->Invalidate();
}
