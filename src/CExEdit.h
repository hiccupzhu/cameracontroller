#if !defined(AFX_CEXEDIT_H__C422E45A_6FCC_46F6_9F9A_F3870C4A1A54__INCLUDED_)
#define AFX_CEXEDIT_H__C422E45A_6FCC_46F6_9F9A_F3870C4A1A54__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CExEdit.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CExEdit window

class CExEdit : public CEdit
{
// Construction
public:
	CExEdit();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExEdit)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CExEdit();

	// Generated message map functions
protected:
	//{{AFX_MSG(CExEdit)
	afx_msg void OnSetfocus();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CEXEDIT_H__C422E45A_6FCC_46F6_9F9A_F3870C4A1A54__INCLUDED_)
