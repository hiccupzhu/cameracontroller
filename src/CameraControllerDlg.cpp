// CameraControllerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CameraController.h"
#include "CameraControllerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "CDlgSet.h"

/////////////////////////////////////////////////////////////////////////////
// CCameraControllerDlg dialog

CCameraControllerDlg::CCameraControllerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCameraControllerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCameraControllerDlg)
	m_nCamera = 0;
	m_nPos = 1;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCameraControllerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCameraControllerDlg)
	DDX_Control(pDX, IDC_EDIT_POS, m_editPos);
	DDX_Control(pDX, IDC_EDIT_CAMERA, m_editCamera);
	DDX_Control(pDX, IDC_DIR_AUTO, m_btnDirAuto);
	DDX_Control(pDX, IDC_DIR_DOWN, m_btnDirDown);
	DDX_Control(pDX, IDC_DIR_LEFTDOWN, m_btnDirLeftDown);
	DDX_Control(pDX, IDC_DIR_RIGHT, m_btnDirRight);
	DDX_Control(pDX, IDC_DIR_RIGHTDOWN, m_btnDirRightDown);
	DDX_Control(pDX, IDC_DIR_RIGHTUP, m_btnDirRightUp);
	DDX_Control(pDX, IDC_DIR_UP, m_btnDirUp);
	DDX_Control(pDX, IDC_DIR_LEFTUP, m_btnDirLeftUp);
	DDX_Control(pDX, IDC_DIR_LEFT, m_btnDirLeft);
	DDX_Control(pDX, IDC_BTN_IRISOPEN, m_btnIrisOpen);
	DDX_Control(pDX, IDC_BTN_IRISCLOSE, m_btnIrisClose);
	DDX_Control(pDX, IDC_BTN_FOCUSNEAR, m_btnFocusNear);
	DDX_Control(pDX, IDC_BTN_FOCUSFAR, m_btnFocusFar);
	DDX_Control(pDX, IDC_BTN_ZOOMWIDE, m_btnZoomWide);
	DDX_Control(pDX, IDC_BTN_ZOOMTELE, m_btnZoomTele);
	DDX_Control(pDX, IDC_BTN_SETPOS, m_btnSetPos);
	DDX_Control(pDX, IDC_BTN_DELPOS, m_btnDelPos);
	DDX_Control(pDX, IDC_BTN_CALLPOS, m_btnCallPos);
	DDX_Control(pDX, IDC_BTN_SET, m_btnSet);
	DDX_Control(pDX, IDC_BTN_COMM, m_btnComm);
	DDX_Control(pDX, IDC_BTN_CLOSE, m_btnClose);
	DDX_Control(pDX, IDC_BTN_MIN, m_btnMin);
	DDX_Control(pDX, IDC_S_POS, m_sPos);
	DDX_Control(pDX, IDC_S_CAMERA, m_sCamera);
	DDX_Control(pDX, IDC_S_AD, m_sAd);
	DDX_Control(pDX, IDC_S_COMINFO, m_sCominfo);
	DDX_Text(pDX, IDC_EDIT_CAMERA, m_nCamera);
	DDV_MinMaxInt(pDX, m_nCamera, 0, 255);
	DDX_Text(pDX, IDC_EDIT_POS, m_nPos);
	DDV_MinMaxInt(pDX, m_nPos, 1, 255);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCameraControllerDlg, CDialog)
	//{{AFX_MSG_MAP(CCameraControllerDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_NCHITTEST()
	ON_BN_CLICKED(IDC_BTN_CLOSE, OnBtnClose)
	ON_BN_CLICKED(IDC_BTN_MIN, OnBtnMin)
	ON_BN_CLICKED(IDC_BTN_COMM, OnBtnComm)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_SET, OnBtnSet)
	ON_EN_CHANGE(IDC_EDIT_CAMERA, OnChangeEditCamera)
	ON_EN_CHANGE(IDC_EDIT_POS, OnChangeEditPos)
	ON_BN_CLICKED(IDC_DIR_AUTO, OnDirAuto)
	ON_BN_CLICKED(IDC_BTN_CALLPOS, OnBtnCallpos)
	ON_BN_CLICKED(IDC_BTN_SETPOS, OnBtnSetpos)
	ON_BN_CLICKED(IDC_BTN_DELPOS, OnBtnDelpos)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCameraControllerDlg message handlers

BOOL CCameraControllerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog

	::DeleteObject(m_hIcon);
	m_hIcon = ::AfxGetApp()->LoadIcon(IDR_MAIN_CLOSE);

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	this->m_szTitle.LoadString(IDS_TITLE);
	this->SetWindowText(this->m_szTitle);

	this->EnableToolTips();
	m_tt.Create(this);
	m_tt.Activate(TRUE);

	
	// TODO: Add extra initialization here
	CRect rect;
	this->GetWindowRect(&rect);

	int nCaption = ::GetSystemMetrics(SM_CYCAPTION);
	int nBorder = ::GetSystemMetrics(SM_CXSIZEFRAME);

	CRgn rgn;
	rgn.CreateRoundRectRgn(nBorder,nCaption+nBorder,
		rect.right-nBorder, rect.bottom-nBorder, 10,10);
	this->SetWindowRgn((HRGN)rgn.m_hObject, TRUE);
	rgn.DeleteObject();
	////////////////////////
	
	this->m_btnMin.SetBitmaps(IDB_MIN, CImgButton::ALIGN_TOP);
	this->m_btnClose.SetBitmaps(IDB_CLOSE, CImgButton::ALIGN_TOP);
	m_tt.AddTool(&m_btnMin, _T("最小化"));
	m_tt.AddTool(&m_btnClose, _T("关闭/退出"));

	this->m_btnComm.SetBitmaps(IDB_BTNLONG);
	this->m_btnComm.SetTextColor(RGB(255,255,255));
	m_tt.AddTool(&m_btnComm, _T("连接串口"));

	this->m_btnSet.SetBitmaps(IDB_BTNLONG);
	this->m_btnSet.SetTextColor(RGB(255,255,255));
	m_tt.AddTool(&m_btnSet, _T("设置串口参数"));

	this->m_btnCallPos.SetBitmaps(IDB_BTNLONG);
	this->m_btnCallPos.SetTextColor(RGB(255,255,255));
	m_tt.AddTool(&m_btnCallPos, _T("调用预置位"));

	this->m_btnSetPos.SetBitmaps(IDB_BTNLONG);
	this->m_btnSetPos.SetTextColor(RGB(255,255,255));
	m_tt.AddTool(&m_btnSetPos, _T("设置预置位"));

	this->m_btnDelPos.SetBitmaps(IDB_BTNLONG);
	this->m_btnDelPos.SetTextColor(RGB(255,255,255));
	m_tt.AddTool(&m_btnDelPos, _T("删除预置位"));

	this->m_btnZoomTele.SetBitmaps(IDB_GOMIN);
	//this->m_btnZoomTele.SetTextColor(RGB(255,255,255));
	m_tt.AddTool(&m_btnZoomTele, _T("缩小"));

	this->m_btnZoomWide.SetBitmaps(IDB_GOMAX);
	//this->m_btnZoomWide.SetTextColor(RGB(255,255,255));
	m_tt.AddTool(&m_btnZoomWide, _T("放大"));


	this->m_btnFocusNear.SetBitmaps(IDB_GOMIN);
	//this->m_btnFocusNear.SetTextColor(RGB(255,255,255));
	m_tt.AddTool(&m_btnFocusNear, _T("焦距缩小"));

	this->m_btnFocusFar.SetBitmaps(IDB_GOMAX);
	//this->m_btnFocusFar.SetTextColor(RGB(255,255,255));
	m_tt.AddTool(&m_btnFocusFar, _T("焦距放大"));


	this->m_btnIrisClose.SetBitmaps(IDB_GOMIN);
	//this->m_btnIrisClose.SetTextColor(RGB(255,255,255));
	m_tt.AddTool(&m_btnIrisClose, _T("光圈关闭"));

	this->m_btnIrisOpen.SetBitmaps(IDB_GOMAX);
	//this->m_btnIrisOpen.SetTextColor(RGB(255,255,255));
	m_tt.AddTool(&m_btnIrisOpen, _T("光圈打开"));


	this->m_btnDirLeft.SetBitmaps(IDB_DIR_LEFT);
	this->m_btnDirLeftUp.SetBitmaps(IDB_DIR_LEFTUP);
	this->m_btnDirLeftDown.SetBitmaps(IDB_DIR_LEFTDOWN);
	this->m_btnDirUp.SetBitmaps(IDB_DIR_UP);
	this->m_btnDirDown.SetBitmaps(IDB_DIR_DOWN);
	this->m_btnDirRightUp.SetBitmaps(IDB_DIR_RIGHTUP);
	this->m_btnDirRight.SetBitmaps(IDB_DIR_RIGHT);
	this->m_btnDirRightDown.SetBitmaps(IDB_DIR_RIGHTDOWN);
	this->m_btnDirAuto.SetBitmaps(IDB_DIR_AUTO);
	m_tt.AddTool(&m_btnDirLeft, _T("左"));
	m_tt.AddTool(&m_btnDirLeftUp, _T("左上"));
	m_tt.AddTool(&m_btnDirLeftDown, _T("左下"));
	m_tt.AddTool(&m_btnDirUp, _T("上"));
	m_tt.AddTool(&m_btnDirDown, _T("下"));
	m_tt.AddTool(&m_btnDirRightUp, _T("右上"));
	m_tt.AddTool(&m_btnDirRight, _T("右"));
	m_tt.AddTool(&m_btnDirRightDown, _T("右下"));
	m_tt.AddTool(&m_btnDirAuto, _T("自动"));
	m_tt.AddTool(&this->m_editCamera, _T("摄像机地址，0至255"));
	m_tt.AddTool(&this->m_editPos, _T("预置位，PelcoD时为1至32，PelcoP时为1至255"));

	//
	this->m_bOpend = FALSE;
	this->m_bFlash = FALSE;
	this->m_bAutoed = FALSE;
	this->m_nLightTimer = 1;

	this->m_sAd.SetTextColor(RGB(0,0,0));
	//this->m_sAd.SetURL("http://");
	m_tt.AddTool(&m_sAd, m_sAd.GetUrl());

	//get saved data


	m_nRates[0] = 2400;
	m_nRates[1] = 4800;
	m_nRates[2] = 9600;
	m_nRates[3] = 14400;
	m_nRates[4] = 19200;

	CWinApp * pWnd = ::AfxGetApp();
	this->m_iniSec = "CommConfig";
	this->m_ini_port = "port";
	this->m_ini_rate = "rate";
	this->m_ini_protocol = "protocol";
	this->m_ini_speedlr = "speedlr";
	this->m_ini_speedud = "speedud";
	this->m_ini_camera = "camera";
	this->m_ini_pos = "pos";

	this->m_nPort = pWnd->GetProfileInt(m_iniSec,m_ini_port,1);	//COM1
	this->m_nRate = pWnd->GetProfileInt(m_iniSec,m_ini_rate,2);;	//9600	
	this->m_nProtocol = pWnd->GetProfileInt(m_iniSec,m_ini_protocol,0);	//Pelco-D
	this->m_nSpeedLr = pWnd->GetProfileInt(m_iniSec,m_ini_speedlr,32);	//
	this->m_nSpeedUd = pWnd->GetProfileInt(m_iniSec,m_ini_speedud,32);
	this->m_nCamera = pWnd->GetProfileInt(m_iniSec,m_ini_camera,0);
	this->m_nPos = pWnd->GetProfileInt(m_iniSec,m_ini_pos,1);

	this->SetCommInfo();
	this->UpdateData(0);

	//去掉启动画面
	this->m_dlgCover.DestroyWindow();
	this->m_btnComm.SetFocus();
	return FALSE;	//TRUE;  // return TRUE  unless you set the focus to a control
}

void CCameraControllerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	//if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	//{
	//	CAboutDlg dlgAbout;
	//	dlgAbout.DoModal();
	//}
	//else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCameraControllerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		
		CPaintDC dc(this);

		CRect rect;
		this->GetClientRect(&rect);
		
		//background
		CBitmap bit;
		bit.LoadBitmap(IDB_BG);
		
		CDC memDC;
		memDC.CreateCompatibleDC(&dc);
		memDC.SelectObject(&bit);
		dc.StretchBlt(1,1,rect.Width(),rect.Height(),
			&memDC,0,0,rect.Width(),rect.Height(),
			SRCCOPY);
		bit.DeleteObject();
		memDC.DeleteDC();

		//caption
		COLORREF oldTextColor = dc.SetTextColor(RGB(255,255,255));
		INT oldBkMode = dc.SetBkMode(TRANSPARENT);
		dc.TextOut(20, 3, this->m_szTitle);
		dc.SetTextColor(oldTextColor);
		dc.SetBkMode(oldBkMode);

		//singnal light
		CRgn rgn;
		CBrush br;

		rgn.CreateEllipticRgn(5,5,19,19);
		br.CreateSolidBrush(RGB(254,254,254));
		dc.FillRgn(&rgn, &br);
		br.DeleteObject();
		rgn.DeleteObject();

		rgn.CreateEllipticRgn(6,6,18,18);

		::DeleteObject(m_hIcon);
		m_hIcon = ::AfxGetApp()->LoadIcon(
			(this->m_bOpend && this->m_bFlash)
			?IDR_MAIN_OPEN:IDR_MAIN_CLOSE);
		this->SetIcon(m_hIcon, TRUE);
		this->SetIcon(m_hIcon, FALSE);

		br.CreateSolidBrush((this->m_bOpend && this->m_bFlash)
			?RGB(255,0,0):RGB(128,0,0));
		dc.FillRgn(&rgn, &br);
		br.DeleteObject();
		rgn.DeleteObject();
		
		this->ReleaseDC(&dc);

		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCameraControllerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CCameraControllerDlg::OnOK() 
{
	//CDialog::OnOK();
}

void CCameraControllerDlg::OnCancel() 
{
	//CDialog::OnCancel();
}

void CCameraControllerDlg::OnClose() 
{	
	this->ShowWindow(SW_HIDE);
	com.Close();
	CDialog::OnOK();
}

LRESULT CCameraControllerDlg::OnNcHitTest(CPoint pt) 
{
	CRect rc;
	this->GetClientRect(&rc);
	this->ClientToScreen(&rc);
	return rc.PtInRect(pt)
		? HTCAPTION : CDialog::OnNcHitTest(pt);
}




void CCameraControllerDlg::OnBtnClose() 
{
	this->OnClose();	
}

void CCameraControllerDlg::OnBtnMin() 
{
	this->ShowWindow(SW_MINIMIZE);
}


void CCameraControllerDlg::OnBtnComm() 
{
	if(!this->m_bOpend) {
		//open comm port;
		if(com.Open(this->m_nPort, this->m_nRates[this->m_nRate])) {
			this->m_btnComm.SetWindowText(_T("断开"));
			this->m_bOpend = TRUE;
		}else{
			CString s;
			s.Format("串口 COM%d 被其他程序占用或不存在", this->m_nPort);
			this->MessageBox(s);
		}
	}else{
		//close comm port;
		com.Close();

		this->m_btnComm.SetWindowText(_T("连接"));
		this->m_bOpend = FALSE;
	}
	this->StopLightFlash();	
}

void CCameraControllerDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == this->m_nLightTimer) {
		this->m_bFlash = !this->m_bFlash;
		CRgn rgn;
		rgn.CreateEllipticRgn(5,5,19,19);
		this->InvalidateRgn(&rgn);
	}
	
	CDialog::OnTimer(nIDEvent);
}

VOID CCameraControllerDlg::StartLightFlash()
{
	if(this->m_bOpend) {
		StopLightFlash();
		this->SetTimer(this->m_nLightTimer, 100, 0);
	}
}

VOID CCameraControllerDlg::StopLightFlash()
{
	this->KillTimer(this->m_nLightTimer);
	this->m_bFlash = this->m_bOpend;

	CRgn rgn;
	rgn.CreateEllipticRgn(5,5,19,19);
	this->InvalidateRgn(&rgn);

	::DeleteObject(m_hIcon);
	m_hIcon = ::AfxGetApp()->LoadIcon(
		this->m_bOpend?IDR_MAIN_OPEN:IDR_MAIN_CLOSE);
	this->SetIcon(m_hIcon, TRUE);
	this->SetIcon(m_hIcon, FALSE);
}


BOOL CCameraControllerDlg::PreTranslateMessage(MSG* pMsg) 
{
	this->m_tt.RelayEvent(pMsg);

	if(this->m_bOpend) {
		CWnd * pWnd = CWnd::FromHandle(pMsg->hwnd);
		UINT msgId = pMsg->message;

		INT nId = 0;
		BYTE b[8];
		b[0] = 0;

		if(msgId == WM_LBUTTONDOWN || msgId == WM_LBUTTONUP) {
			//set protocol
			if(this->m_nProtocol == 0) {
				action.SetPelcoD();
			}else{
				action.SetPelcoP();
			}

			this->UpdateData();
			action.SetAddress(this->m_nCamera);
			
			if(strcmp(pWnd->GetRuntimeClass()->m_lpszClassName,"CButton")==0) {
				nId = pWnd->GetDlgCtrlID();
			}
		}
		switch (nId) {
		case IDC_DIR_UP:
		case IDC_DIR_DOWN:
		case IDC_DIR_LEFTDOWN:
		case IDC_DIR_RIGHTDOWN:
		case IDC_DIR_LEFT:
		case IDC_DIR_RIGHT:
		case IDC_DIR_LEFTUP:
		case IDC_DIR_RIGHTUP:
		case IDC_BTN_ZOOMTELE:
		case IDC_BTN_ZOOMWIDE:
		case IDC_BTN_FOCUSNEAR:
		case IDC_BTN_FOCUSFAR:
		case IDC_BTN_IRISOPEN:
		case IDC_BTN_IRISCLOSE:
			if(msgId == WM_LBUTTONDOWN) {
				if(this->m_bAutoed) {
					this->OnDirAuto();
				}

				pWnd->SetCapture();
				this->StartLightFlash();

				action.ToUp(b, this->m_nSpeedUd, nId == IDC_DIR_UP);
				action.ToDown(b, this->m_nSpeedUd, nId == IDC_DIR_DOWN);
				action.ToLeft(b, this->m_nSpeedLr, nId == IDC_DIR_LEFT);
				action.ToRight(b, this->m_nSpeedLr, nId == IDC_DIR_RIGHT);

				action.ToLeftUp(b, this->m_nSpeedLr, this->m_nSpeedUd, nId == IDC_DIR_LEFTUP);
				action.ToLeftDown(b, this->m_nSpeedLr, this->m_nSpeedUd, nId == IDC_DIR_LEFTDOWN);
				action.ToRightUp(b, this->m_nSpeedLr, this->m_nSpeedUd, nId == IDC_DIR_RIGHTUP);
				action.ToRightDown(b, this->m_nSpeedLr, this->m_nSpeedUd, nId == IDC_DIR_RIGHTDOWN);

				action.ZoomTele(b, nId == IDC_BTN_ZOOMTELE);
				action.ZoomWide(b, nId == IDC_BTN_ZOOMWIDE);
				action.FocusNear(b, nId == IDC_BTN_FOCUSNEAR);
				action.FocusFar(b, nId == IDC_BTN_FOCUSFAR);
				action.IrisClose(b, nId == IDC_BTN_IRISCLOSE);
				action.IrisOpen(b, nId == IDC_BTN_IRISOPEN);

			}else if(msgId == WM_LBUTTONUP) {
				::ReleaseCapture();
				this->StopLightFlash();

				action.Stop(b);
			}
			if(b[0]!=0) {
				com.Write(b, action.GetBytesSize());
			}
			break;
		default:
			;
		}
	}
	
	
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CCameraControllerDlg::OnBtnSet() 
{
	CDlgSet dlg;
	dlg.m_nPort = this->m_nPort - 1;
	dlg.m_nRate = this->m_nRate;
	dlg.m_nProtocol = this->m_nProtocol;
	dlg.m_szSpeedLr.Format("%d", this->m_nSpeedLr);
	dlg.m_szSpeedUd.Format("%d", this->m_nSpeedUd);

	int result = dlg.DoModal();
	if(result == IDOK) {
		dlg.m_nPort = dlg.m_nPort + 1;
		
		if((dlg.m_nPort != this->m_nPort || dlg.m_nRate != this->m_nRate)
			&& this->m_bOpend) {
			this->m_nPort = dlg.m_nPort;
			this->m_nRate = dlg.m_nRate;

			this->OnBtnComm();
			this->OnBtnComm();
		}
		this->m_nPort = dlg.m_nPort;
		this->m_nRate = dlg.m_nRate;
		this->m_nProtocol = dlg.m_nProtocol;
		this->m_nSpeedLr = atoi(dlg.m_szSpeedLr);
		this->m_nSpeedUd = atoi(dlg.m_szSpeedUd);
		this->SetCommInfo();
		CWinApp * p = ::AfxGetApp();
		p->WriteProfileInt(m_iniSec, m_ini_port, m_nPort);
		p->WriteProfileInt(m_iniSec, m_ini_rate, m_nRate);
		p->WriteProfileInt(m_iniSec, m_ini_protocol, m_nProtocol);
		p->WriteProfileInt(m_iniSec, m_ini_speedlr, m_nSpeedLr);
		p->WriteProfileInt(m_iniSec, m_ini_speedud, m_nSpeedUd);

		if(this->m_nProtocol==0) {
			this->OnChangeEditPos();
		}
	}
}

VOID CCameraControllerDlg::SetCommInfo()
{
	CString s;
	s.Format("COM%d/%d/%s", this->m_nPort, this->m_nRates[this->m_nRate], 
		this->m_nProtocol==0?"Pelco-D":"Pelco-P");
	this->m_sCominfo.SetWindowText(s);
	this->Invalidate();
}

void CCameraControllerDlg::OnChangeEditCamera() 
{
	CString s;
	this->m_editCamera.GetWindowText(s);

	if(s.GetLength()==0) {
		this->m_editCamera.SetWindowText("0");
		this->m_nCamera = 0;
	}else if(atoi(s)<0) {
		this->m_editCamera.SetWindowText("0");
		this->m_nCamera = 0;
	}else if(atoi(s)>255) {
		this->m_editCamera.SetWindowText("255");
		this->m_nCamera = 255;
	}else if(this->m_nCamera != atoi(s)) {
		this->UpdateData();
	}
	AfxGetApp()->WriteProfileInt(m_iniSec, m_ini_camera, m_nCamera);
}

void CCameraControllerDlg::OnChangeEditPos() 
{
	CString s;
	this->m_editPos.GetWindowText(s);

	if(s.GetLength()==0) {
		this->m_editPos.SetWindowText("1");
		this->m_nPos = 1;
		s = "1";
	}

	if(this->m_nProtocol == 0) {
		action.SetPelcoD();
	}else{
		action.SetPelcoP();
	}

	int pos = action.GetCorrectPos(atoi(s));
	s.Format("%d",pos);
	this->m_editPos.SetWindowText(s);

	if(this->m_nPos != pos) {
		this->UpdateData();
		AfxGetApp()->WriteProfileInt(m_iniSec, m_ini_pos, m_nPos);
	}
}

void CCameraControllerDlg::OnDirAuto() 
{
	if(!this->m_bOpend) {
		return;
	}
	this->StopCameraAction();

	BYTE b[8];
	b[0] = 0;

	if(this->m_bAutoed) {
		this->m_btnDirAuto.SetState();
		this->m_bAutoed = FALSE;
		this->m_tt.AddTool(&this->m_btnDirAuto, _T("自动"));
	}else{
		//not auto
		//stop previous action

		action.AutoStart(b, this->m_nSpeedLr, this->m_nSpeedUd);
		com.Write(b, action.GetBytesSize());

		this->m_btnDirAuto.SetState(CImgButton::STATE_CLICKED);
		this->StartLightFlash();
		this->m_tt.AddTool(&this->m_btnDirAuto, _T("自动，正在运行，点击停止"));
		this->m_bAutoed = TRUE;
	}
}

void CCameraControllerDlg::OnBtnCallpos() 
{
	if(!this->m_bOpend) {
		return;
	}
	this->StopCameraAction();

	BYTE b[8];
	b[0] = 0;
	
	this->UpdateData();

	this->m_nPos = action.GetCorrectPos(this->m_nPos);

	this->UpdateData(0);

	action.CallPreset(b, this->m_nPos);

	com.Write(b, action.GetBytesSize());
}

void CCameraControllerDlg::OnBtnSetpos() 
{
	if(!this->m_bOpend) {
		return;
	}
	this->StopCameraAction();

	BYTE b[8];
	b[0] = 0;
	
	this->UpdateData();

	this->m_nPos = action.GetCorrectPos(m_nPos);

	this->UpdateData(0);

	action.SetPreset(b, this->m_nPos);

	com.Write(b, action.GetBytesSize());
}

void CCameraControllerDlg::OnBtnDelpos() 
{
	if(!this->m_bOpend) {
		return;
	}
	this->StopCameraAction();

	BYTE b[8];
	b[0] = 0;
	
	this->UpdateData();

	this->m_nPos = action.GetCorrectPos(m_nPos);

	this->UpdateData(0);

	action.DelPreset(b, this->m_nPos);

	com.Write(b, action.GetBytesSize());
}

VOID CCameraControllerDlg::StopCameraAction()
{
	BYTE b[8];
	b[0] = 0;

	if(this->m_nProtocol == 0) {
		action.SetPelcoD();
	}else{
		action.SetPelcoP();
	}

	this->UpdateData();
	
	::ReleaseCapture();
	this->StopLightFlash();
	action.Stop(b);
	com.Write(b, action.GetBytesSize());
}
