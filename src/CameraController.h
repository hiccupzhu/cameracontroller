// CameraController.h : main header file for the CameraController application
//

#if !defined(AFX_CameraController_H__95790D5D_D1BE_49C4_805B_36682A23F548__INCLUDED_)
#define AFX_CameraController_H__95790D5D_D1BE_49C4_805B_36682A23F548__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCameraControllerApp:
// See CameraController.cpp for the implementation of this class
//

class CCameraControllerApp : public CWinApp
{
public:
	CCameraControllerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCameraControllerApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCameraControllerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CameraController_H__95790D5D_D1BE_49C4_805B_36682A23F548__INCLUDED_)
