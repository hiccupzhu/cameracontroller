// CExEdit.cpp : implementation file
//

#include "stdafx.h"
#include "CameraController.h"
#include "CExEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExEdit

CExEdit::CExEdit()
{

}

CExEdit::~CExEdit()
{
}


BEGIN_MESSAGE_MAP(CExEdit, CEdit)
	//{{AFX_MSG_MAP(CExEdit)
	ON_CONTROL_REFLECT(EN_SETFOCUS, OnSetfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExEdit message handlers


void CExEdit::OnSetfocus() 
{
	this->SetSel(0,-1);
}


