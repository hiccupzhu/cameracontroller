#if !defined(AFX_CBASEBUTTON_H__36894AEA_CFA9_4BF3_9D93_455E54F93811__INCLUDED_)
#define AFX_CBASEBUTTON_H__36894AEA_CFA9_4BF3_9D93_455E54F93811__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CBaseButton.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBaseButton window

class CBaseButton : public CButton
{
// Construction
public:
	CBaseButton();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBaseButton)
	//}}AFX_VIRTUAL

// Implementation
public:
	static VOID TransparentBlt( CDC* hdcDest,        // 目标DC
                     int nXOriginDest,   // 目标X偏移
                     int nYOriginDest,   // 目标Y偏移
                     int nWidthDest,     // 目标宽度
                     int nHeightDest,    // 目标高度
                     CDC* hdcSrc,         // 源DC
                     int nXOriginSrc,    // 源X起点
                     int nYOriginSrc,    // 源Y起点
                     int nWidthSrc,      // 源宽度
                     int nHeightSrc,     // 源高度
                     UINT crTransparent  // 透明色,COLORREF类型
                );

	virtual ~CBaseButton();

	// Generated message map functions
protected:
	UINT m_nIDTimerHover;
	BOOL m_bHover;
	//{{AFX_MSG(CBaseButton)
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	HRGN hRgn;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CBASEBUTTON_H__36894AEA_CFA9_4BF3_9D93_455E54F93811__INCLUDED_)
