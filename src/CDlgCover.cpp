// CDlgCover.cpp : implementation file
//

#include "stdafx.h"
#include "CameraController.h"
#include "CDlgCover.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgCover dialog


CDlgCover::CDlgCover(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgCover::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgCover)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgCover::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgCover)
	DDX_Control(pDX, IDC_S_COVER, m_sCover);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgCover, CDialog)
	//{{AFX_MSG_MAP(CDlgCover)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgCover message handlers

void CDlgCover::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	//CDialog::OnCancel();
}

void CDlgCover::OnOK() 
{
	// TODO: Add extra validation here
	
	//CDialog::OnOK();
}

void CDlgCover::OnPaint() 
{
	this->m_sCover.SetTextColor(RGB(255,255,255));
	CPaintDC dc(this); // device context for painting
	
	CBrush br;
	br.CreateSolidBrush(RGB(100,100,100));
	CRect rc;
	this->GetClientRect(&rc);
	dc.FillRect(&rc, &br);
	br.DeleteObject();

	// Do not call CDialog::OnPaint() for painting messages
}

